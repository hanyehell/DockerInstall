# DockerInstall

#### 介绍
    自己整理的DockerProject,整理的Dockerfile或者docker-compose

#### 软件架构
    Docker          Version: 18.06.1-ce
    docker-compose  Version：1.23.2

#### 安装教程
##### docker-ce
    [root@k8s-node01 ~]# wget  https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo 
    [root@k8s-node01 ~]# mv docker-ce.repo  /etc/yum.repos.d/
    [root@k8s-node01 yum.repos.d]# yum install -y docker-ce
    [root@k8s-node01 ~]# systemctl  daemon-reload
    [root@k8s-node01 ~]# systemctl  start docker  
##### docker-compose
    参考：https://docs.docker.com/compose/install/

#### 使用说明
   docker 直接下载项目 docker build -t varsName .
   docker-compose  直接下载项目 docker-compose -f Docker-compose.file up -d
