#### 1 规划
    软件：PHP 7.2、Nginx、MySQL、Composer、NPM AND  Yarn 等等；
    易于扩展使用，如随时可以切换 PHP 版本，或者 Apache 和 Nginx 切换使用。
    规划：  先在centos镜像安装php-fpm和laravel的需要依赖包，使用supervisord监控php-fpm的启动，然后通过挂载项目的目录到容器之内   执行脚本构建项目。实现项目发布和更新迭代。
    此处我仅仅处理了lavravel的php和依赖环境，并没有处理nginx和mysql的环境，后期会出关于其他依赖的环境的操作。
#### 环境版本
    php-fpm 7.2
    node-v10.10.0
    yarn-v1.13.0-1
    nodejs 8
#### 基于镜像 
   centos 镜像

#### php 7.2
     wget -c http://mirrors.linuxeye.com/oneinstack-full.tar.gz && tar xzf oneinstack-full.tar.gz && ./oneinstack/install.sh --php_option 7 --phpcache_option 1 --php_extensions zendguardloader,ioncube,imagick,fileinfo,redis,swoole
#### node
    wget https://nodejs.org/download/release/v10.10.0/node-v10.10.0-linux-x64.tar.gz
#### yarn
    wget install -y https://github.com/yarnpkg/yarn/releases/download/v1.13.0/yarn-1.13.0-1.noarch.rpm
    yum install -y yarn-1.13.0-1.noarch.rpm
#### nodejs
    curl -sL https://rpm.nodesource.com/setup_8.x | bash -
#### composer
    cd /usr/local/bin
    curl -s https://getcomposer.org/installer | php
    chmod a+x composer.phar     
    composer.phar self-update      
    mv composer.phar composer
#### supervisor安装
   监控php-fpm  我这里没有写 可以参考：https://blog.51cto.com/9025736/2433204
#### 启动
    docker run -itd  --name=test3 --hostname=php72 --volume=/data/cloud_php/startphp-fpm.sh:/root/startphp-fpm.sh --volume=/data/cloud_php/supervisord.conf:/etc/supervisord.conf --volume=/home/qa/fx_store:/home/qa/fx_store --volume=/home/qa/fx_store/public:/home/qa/fx_store/public --restart=no  hanye131/fangxinerp:supvisor_php-fpm72
#### startphp-fpm.sh
    #!/bin/bash
    supervisord  -n -c /etc/supervisord.conf
#### supervisord.conf

[unix_http_server]
file=/var/run/supervisor/supervisor.sock   ; (the path to the socket file)

[supervisord]
logfile=/var/log/supervisor/supervisord.log  ; (main log file;default $CWD/supervisord.log)
logfile_maxbytes=50MB       ; (max main logfile bytes b4 rotation;default 50MB)
logfile_backups=10          ; (num of main logfile rotation backups;default 10)
loglevel=info               ; (log level;default info; others: debug,warn,trace)
pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
nodaemon=false              ; (start in foreground if true;default false)
minfds=1024                 ; (min. avail startup file descriptors;default 1024)
minprocs=200                ; (min. avail process descriptors;default 200)

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///var/run/supervisor/supervisor.sock ; use a unix:// URL  for a unix socket
;serverurl=http://127.0.0.1:9001 ; use an http:// url to specify an inet socket
;username=chris              ; should be same as http_username if set
;password=123                ; should be same as http_password if set
;prompt=mysupervisor         ; cmd line prompt (default "supervisor")
;history_file=~/.sc_history  ; use readline history if available

; The below sample program section shows all possible program subsection values,
; create one or more 'real' program: sections to be able to control them under
; supervisor.


[program:php-fpm]
process_name=%(program_name)s_%(process_num)02d
command=/usr/local/php/sbin/php-fpm --fpm-config /usr/local/php/etc/php-fpm.conf
#stdout_logfile=/data/wwwlogs/php-fpm-sup.log
autostart=true
autorestart=true
startretries=5
; startsecs=0
exitcodes=0,2,70
stopsignal=QUIT
stopwaitsecs=2

[include]
files = supervisord.d/*.ini